PACKAGE
=======

.. automodule:: __PACKAGE_DIR__
   :members:
   :undoc-members:
   :show-inheritance:

abc
---

.. automodule:: __PACKAGE_DIR__.abc
   :members:
   :undoc-members:
   :show-inheritance:

etc
---

.. automodule:: __PACKAGE_DIR__.etc
   :members:
   :undoc-members:
   :show-inheritance:

services
--------

.. automodule:: __PACKAGE_DIR__.services
   :members:
   :undoc-members:
   :show-inheritance:
