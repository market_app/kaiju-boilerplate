import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()


with open(os.path.join(here, 'requirements.txt')) as f:
    req_lines = [line.strip() for line in f.readlines()]

    def fix_url_line(line):
        if '#egg=' in line:
            return line.rsplit('#egg=', 1)[1]
        return line
    requires = [fix_url_line(line) for line in req_lines if line]


setup(
    name='__PACKAGE_NAME__',
    version='__PACKAGE_VERSION__',
    description='__PACKAGE_DESCRIPTION__',
    long_description=README + '\n\n' + CHANGES,
    long_description_content_type='text/markdown; charset=UTF-8',
    author='__EMAIL__',
    author_email='__EMAIL__',
    url='https://bitbucket.org/market_app/__PACKAGE_NAME__',
    packages=find_packages(),
    package_dir={'__PACKAGE_DIR__': '__PACKAGE_DIR__'},
    include_package_data=True,
    python_requires=">=__PYTHON_VERSION__",
    install_requires=requires,
    license='LICENSE.txt',
    zip_safe=False,
    classifiers=(
        'Programming Language :: Python :: __PYTHON_VERSION__',
    ),
    keywords=['elemento', 'kaiju']
)
