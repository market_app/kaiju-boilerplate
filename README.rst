How to use this boilerplate for your new project
------------------------------------------------

Just copy or pull all the boilerplate files to your new repository.

.. code-block::

    cd my-project
    git init
    git pull git@bitbucket.org:market_app/kaiju-boilerplate.git

Add your python package to the root or rename the existing one. Use IDE `replace` tool
to replace variables.

=======================  ============================================================
   Name                     Description
=======================  ============================================================
__PACKAGE_NAME__            package name as in pip repository (example: some-stuff)
__PACKAGE_VERSION__         package version (example: 0.1.0)
__PACKAGE_DESCRIPTION__     package short description
__PYTHON_VERSION__          minimum python version requirement (example: 3.6.1)
__EMAIL__                   author's email
__PACKAGE_DIR__             package root directory where `__init__.py` is located
=======================  ============================================================

.. caution::

    It's better to create a python virtual enviroment after and not before you've
    replaced variables. Otherwise it's possible to accidentally replace text in
    virtualenv packages and produce errors.

After you have done all that, you will be able to install your package using pip command
wherever you want.

.. code-block::

    pip install -e git+git@bitbucket.org:market_app/my-project.git#egg=my_project


Compatibility
-------------

**Python**: __PYTHON_VERSION__

Summary
-------

*__LIST YOUR MODULES/PACKAGES WITH SHORT DESCRIPTION OF EACH__*

Testing
-------

At first, you must install `requirements.tests.txt` as well
as normal requirements.

pytest
^^^^^^

Run `pytest` command. There's also a Pycharm *unittests*
run configuration ready to use.

tox
^^^

To test with tox you should install and use `pyenv`. First
setup local interpreters which you want to use in tests.

```pyenv local 3.7.5 3.8.1 3.9.0```

Then you can run `tox` command to test against all of them.

Documentation
-------------

sphinx
^^^^^^

Install `requirements.docs.txt` and then
cd to `./docs` and run `make html` command. There is also a
run configuration for Pycharm.
