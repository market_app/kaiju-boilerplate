"""
This module SHOULD usually contain only the package meta-information and
imports from the package's modules.

This docstring SHOULD contain the general package description and the userguide,
because this section is shown the first in the package index.
"""

__version__ = '__PACKAGE_VERSION__'
__python_version__ = '__PYTHON_VERSION__'
__author__ = '__EMAIL__'
