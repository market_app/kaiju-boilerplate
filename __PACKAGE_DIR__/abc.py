"""
This module SHOULD contain class interfaces or MAY contain some base classes
as well (but it's still better to implement base classes in modules you use them).

Class interfaces may be used for type hinting or service discovery by services
from other packages.

For example, implement your AbstractService with method stubs here and inherit
your service from it.

in .abc.py:

.. code-block::

    import abc

    from kaiju_tools.services import Service

    class AbstractSomethingService(Service, abc.ABC):

        service_name = 'something'

        @abc.abstractmethod
        def do_something(self, a: int, b: int) -> int:
            pass


in .something_service.py:

.. code-block::

    import abc

    from .abc import AbstractSomethingService

    class SomethingService(AbstractSomethingService):

        def do_something(self, a: int, b: int) -> int:
            return int(a * b)


"""
